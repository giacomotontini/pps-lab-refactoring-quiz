package com.geoquiz.view.menu;

import com.geoquiz.view.utility.Background;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.ButtonsCategory;
import com.geoquiz.view.button.MyButton;
import com.geoquiz.view.button.MyButtonFactory;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelFactory;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * The scene where user can choose game category.
 */
public class CategoryScene extends Scene {

    private static final double POS_1_X = 100;
    private static final double POS_1_Y = 450;
    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_X_BACK = 450;
    private static final double POS_Y_BACK = 600;
    private static final double BUTTON_WIDTH = 350;
    private static final Text BUTTON_PRESSED = new Text();
    private static final double USER_LABEL_FONT = 40;

    private final Pane panel = new Pane();

    private final HBox hbox = new HBox(10);
    private final HBox hbox2 = new HBox(10);
    private final VBox vbox = new VBox();

    public final Stage mainStage;
    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public CategoryScene(final Stage mainStage) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        final List<MyButton> buttons = new ArrayList<>();
        final MyButton backButton = MyButtonFactory.createMyButton(Buttons.BACK.toString(), Color.BLUE, BUTTON_WIDTH);
        final MyButton capitals = MyButtonFactory.createMyButton(ButtonsCategory.CAPITALS.toString(), Color.BLUE, BUTTON_WIDTH);
        final MyButton currencies = MyButtonFactory.createMyButton(ButtonsCategory.CURRENCIES.toString(), Color.BLUE, BUTTON_WIDTH);
        final MyButton kitchen = MyButtonFactory.createMyButton(ButtonsCategory.KITCHEN.toString(), Color.BLUE, BUTTON_WIDTH);
        final MyButton monuments = MyButtonFactory.createMyButton(ButtonsCategory.MONUMENTS.toString(), Color.BLUE, BUTTON_WIDTH);
        final MyButton flags = MyButtonFactory.createMyButton(ButtonsCategory.FLAGS.toString(), Color.BLUE, BUTTON_WIDTH);
        buttons.add(capitals);
        buttons.add(currencies);
        buttons.add(kitchen);
        buttons.add(monuments);
        buttons.add(flags);


        final MyLabel userLabel = MyLabelFactory.createMyLabel("USER: " + LoginMenuScene.getUsername(), Color.BLACK,
                USER_LABEL_FONT);

        hbox.setTranslateX(POS_1_X);
        hbox.setTranslateY(POS_1_Y);

        hbox2.setTranslateX(POS_2_X);
        hbox2.setTranslateY(POS_2_Y);

        vbox.setTranslateX(POS_X_BACK);
        vbox.setTranslateY(POS_Y_BACK);

        hbox.getChildren().addAll((Node) flags, (Node) currencies, (Node) kitchen);
        hbox2.getChildren().addAll((Node) monuments, (Node) capitals);
        vbox.getChildren().add((Node) backButton);

        this.mainStage = mainStage;

        ((Node) backButton).setOnMouseClicked(event -> {
            reproduceClickSound();
            try {
                mainStage.setScene(new MainMenuScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        buttons.forEach(b -> ((Node) b).setOnMouseClicked(event -> sceneChange()));

        this.panel.getChildren().addAll(Background.getImage(), Background.createBackground(), hbox, hbox2, vbox,
                Background.getLogo(), (Node) userLabel);

        this.setRoot(this.panel);
    }

    /**
     * @return category.
     */
    public static String getCategoryPressed() {
        return BUTTON_PRESSED.getText();
    }

    private void sceneChange(){
        reproduceClickSound();
        BUTTON_PRESSED.setText(getCategoryPressed());
        mainStage.setScene(new ModeScene(mainStage));
    }

    private void reproduceClickSound(){
        if (!MainWindow.isWavDisabled()) {
            MainWindow.playClick();
        }
    }

}
