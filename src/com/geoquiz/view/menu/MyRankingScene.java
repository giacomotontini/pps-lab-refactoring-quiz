package com.geoquiz.view.menu;

import java.io.IOException;
import java.util.Map;

import javax.xml.bind.JAXBException;

import com.geoquiz.utility.Pair;
import com.geoquiz.view.button.ButtonsCategory;

import javafx.stage.Stage;

/**
 * The statistics scene where user can see own records.
 */
public class MyRankingScene extends AbsoluteRankingScene {

    private Map<Pair<String, String>, Integer> map;

    /**
     * @param mainStage
     *            the stage where the scene is called.
     * @throws JAXBException
     *             for xml exception.
     */
    public MyRankingScene(final Stage mainStage) throws JAXBException {
        super(mainStage);
        super.getTitle().setText("My records");
        try {
            map = super.getRanking().getPersonalRanking(LoginMenuScene.getUsername());
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        super.clearLabel();

        super.getCapitalsEasy().setText(super.getCapitalsEasy().getText()
                + this.getRecordByCategory(ButtonsCategory.CAPITALS.toString(), ButtonsCategory.EASY.toString()));
        super.getCapitalsMedium().setText(super.getCapitalsMedium().getText()
                + this.getRecordByCategory(ButtonsCategory.CAPITALS.toString(), ButtonsCategory.MEDIUM.toString()));
        super.getCapitalsHard().setText(super.getCapitalsHard().getText()
                + this.getRecordByCategory(ButtonsCategory.CAPITALS.toString(), ButtonsCategory.HARD.toString()));
        super.getCapitalsChallenge().setText(super.getCapitalsChallenge().getText()
                + this.getRecordByCategory(ButtonsCategory.CAPITALS.toString(), ButtonsCategory.CHALLENGE.toString()));
        super.getMonumentsEasy().setText(super.getMonumentsEasy().getText()
                + this.getRecordByCategory(ButtonsCategory.MONUMENTS.toString(), ButtonsCategory.EASY.toString()));
        super.getMonumentsMedium().setText(super.getMonumentsMedium().getText()
                + this.getRecordByCategory(ButtonsCategory.MONUMENTS.toString(), ButtonsCategory.MEDIUM.toString()));
        super.getMonumentsHard().setText(super.getMonumentsHard().getText()
                + this.getRecordByCategory(ButtonsCategory.MONUMENTS.toString(), ButtonsCategory.HARD.toString()));
        super.getMonumentsChallenge().setText(super.getMonumentsChallenge().getText()
                + this.getRecordByCategory(ButtonsCategory.MONUMENTS.toString(), ButtonsCategory.CHALLENGE.toString()));
        super.getFlagsClassic().setText(super.getFlagsClassic().getText()
                + this.getRecordByCategory(ButtonsCategory.FLAGS.toString(), ButtonsCategory.CLASSICS.toString()));
        super.getFlagsChallenge().setText(super.getFlagsChallenge().getText()
                + this.getRecordByCategory(ButtonsCategory.FLAGS.toString(), ButtonsCategory.CHALLENGE.toString()));
        super.getCurrenciesClassic().setText(super.getCurrenciesClassic().getText()
                + this.getRecordByCategory(ButtonsCategory.CURRENCIES.toString(), ButtonsCategory.CLASSICS.toString()));
        super.getCurrenciesChallenge().setText(super.getCurrenciesChallenge().getText()
                + this.getRecordByCategory(ButtonsCategory.CURRENCIES.toString(), ButtonsCategory.CHALLENGE.toString()));
        super.getDishesClassic().setText(super.getDishesClassic().getText()
                + this.getRecordByCategory(ButtonsCategory.KITCHEN.toString(), ButtonsCategory.CLASSICS.toString()));
        super.getDishesChallenge().setText(super.getDishesChallenge().getText()
                + this.getRecordByCategory(ButtonsCategory.KITCHEN.toString(), ButtonsCategory.CHALLENGE.toString()));

    }

    private String getRecordByCategory(final String category, final String difficulty) {
        final Integer record = this.map.get(new Pair<>(category, difficulty));
        return record == null ? "" : record.toString();
    }
}