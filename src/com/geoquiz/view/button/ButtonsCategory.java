package com.geoquiz.view.button;

/**
 * The menu's buttons who a player can press to choose category and modality
 * game.
 */
public enum ButtonsCategory {
    /**
     * Represents the category "Capitali".
     */
    CAPITALS,
    /**
     * Represents the category "Valute".
     */
    CURRENCIES,
    /**
     * Represents the category "Typical dishes".
     */
    KITCHEN,
    /**
     * Represents the category "Bandiere".
     */
    FLAGS,
    /**
     * Represents the category "Monumenti".
     */
    MONUMENTS,
    /**
     * Represents the difficulty level "Facile".
     */
    EASY,
    /**
     * Represents the difficulty level "Medio".
     */
    MEDIUM,
    /**
     * Represents the difficulty level "Difficile".
     */
    HARD,
    /**
     * Represents the game mode "Classica".
     */
    CLASSICS,
    /**
     * Represents the game mode "Sfida".
     */
    CHALLENGE,
    /**
     * Represents the game mode "Allenamento".
     */
    TRAINING;

}
