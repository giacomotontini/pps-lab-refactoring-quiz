package com.geoquiz.view.button;

/**
 * The menu's buttons who a player can press.
 */
public enum Buttons {
    /**
     * Log in if user is authenticted.
     */
    LOGIN,
    /**
     * Possibility to create an account.
     */
    SIGNUP,
    /**
     * Exit the game.
     */
    EXIT,
    /**
     * Go to scene before.
     */
    BACK,
    /**
     * Enter in category scene.
     */
    PLAY,
    /**
     * Enter in option scene.
     */
    OPTIONS,
    /**
     * Register a new accont.
     */
    SAVE,
    /**
     * Enter in ranking scene.
     */
    CHARTS,
    /**
     * Enter in statistics scene.
     */
    STATS,

    /**
     * Enter in instructions scene.
     */
    INSTRUCTIONS;

}
